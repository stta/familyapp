﻿using MongoDB.Driver;
using St.Family.Models;
using St.Family.Models.Person;
using System.Collections.Generic;
using System.Linq;

namespace St.Family.Service
{
    public class PersonService
    {
        private readonly IMongoCollection<Person> _persons;

        public PersonService(IFamilyDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _persons = database.GetCollection<Person>(settings.FamilyCollectionName);
        }

        public List<Person> Get() => _persons.Find(person => true).ToList();
        public Person Get(string id) => _persons.Find(person => person.Id == id).First();

        public Person Create(Person person)
        {
            _persons.InsertOne(person);
            return person;
        }

        public void Update(string id, Person person)
        {
            _persons.ReplaceOne(p => p.Id == id, person);
        }

        public void Remove(Person person)
        {
            _persons.DeleteOne(p => p.Id == person.Id);
        }

        public void Remove(string id)
        {
            _persons.DeleteOne(p => p.Id == id);
        }

    }
}
