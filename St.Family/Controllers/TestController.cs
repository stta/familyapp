﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using St.Family.Models.Person;
using St.Family.Service;
using System.Collections.Generic;

namespace St.Family.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly PersonService _personService;

        public TestController(PersonService context)
        {
            _personService = context;
        }

        [HttpGet]
        public ActionResult<List<Person>> GetPersons() => _personService.Get();

        //public ActionResult<Person> Get(string id)
        //{
        //    var person = _personService.Get(id);
        //    if (person == null)
        //    {
        //        return NotFound();
        //    }

        //    return person;
        //}

        [HttpPost]
        public ActionResult<Person> Create(Person person)
        {
            _personService.Create(person);

            return CreatedAtRoute("GetPerson", new { id = person.Id.ToString() }, person);
        }

    }
}
