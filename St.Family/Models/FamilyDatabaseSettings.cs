﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace St.Family.Models
{
    public class FamilyDatabaseSettings : IFamilyDatabaseSettings
    {
        public string FamilyCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IFamilyDatabaseSettings
    {
       string  FamilyCollectionName { get; set; }
       string  ConnectionString { get; set; }
       string  DatabaseName { get; set; }
    }
}
