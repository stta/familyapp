﻿using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace St.Family.Models.User
{
    public class FamilyContext
    {
        public IMongoDatabase database; // база данных
        public IGridFSBucket gridFS;   // файловое хранилище

        public FamilyContext()
        {
            string connectionString = "mongodb://localhost:27017/family";
            var connection = new MongoUrlBuilder(connectionString);
            // получаем клиента для взаимодействия с базой данных
            MongoClient client = new MongoClient(connectionString);
            // получаем доступ к самой базе данных
            database = client.GetDatabase(connection.DatabaseName);
            // получаем доступ к файловому хранилищу
            gridFS = new GridFSBucket(database);
        }

        public IMongoCollection<User> Users
        {
            get { return database.GetCollection<User>("Users"); }
        }

        public async Task<IEnumerable<User>> GetUserByFilter(FilterDefinition<User> filter)
        {
            return await Users.Find(filter).ToListAsync();
        }
    }
}
