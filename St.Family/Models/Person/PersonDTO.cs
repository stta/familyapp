﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace St.Family.Models.Person
{
    public class PersonDTO
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
